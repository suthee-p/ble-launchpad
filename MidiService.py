from pybleno import *
from MidiCharacteristic import *

class MidiService(BlenoPrimaryService):
    def __init__(self):
        self.launchpadState = None
        self.midiCharacteristic = MidiCharacteristic()
        BlenoPrimaryService.__init__(self, {
            'uuid': '03B80E5A-EDE8-4B33-A751-6CE34EC4C700',
            'characteristics': [
                self.midiCharacteristic
            ]
        })

    def setLaunchpadState(self, launchpadState):
        self.launchpadState = launchpadState
        self.midiCharacteristic.setLaunchpadState(launchpadState)