from pygame import time
import math
import Util as util

#     +---+---+---+---+---+---+---+---+ 
#     |104|   |   |   |   |   |   |111|
# +---+---+---+---+---+---+---+---+---+---+
# | 80| 81|   |   |   |   |   |   | 88| 89|
# +---+---+---+---+---+---+---+---+---+---+
# | 70|   | 72|   |   |   |   | 77|   | 79|
# +---+---+---+---+---+---+---+---+---+---+
# | 60|   |   | 63|   |   | 66|   |   | 69|
# +---+---+---+---+---+---+---+---+---+---+
# | 50|   |   |   | 54| 55|   |   |   | 59|
# +---+---+---+---+---+---+---+---+---+---+
# | 40|   |   |   | 44| 45|   |   |   | 49|
# +---+---+---+---+---+---+---+---+---+---+
# | 30|   |   | 33|   |   | 36|   |   | 39|
# +---+---+---+---+---+---+---+---+---+---+
# | 20|   | 22|   |   |   |   | 27|   | 29|
# +---+---+---+---+---+---+---+---+---+---+
# | 10| 11|   |   |   |   |   |   | 18| 19|
# +---+---+---+---+---+---+---+---+---+---+
#     |  1|   |   |   |   |   |   |  8|
#     +---+---+---+---+---+---+---+---+ 

class LaunchpadState:
    ModePad    = 0
    ModeRaw         = 1
    ModeString      = ['Pad', 'Raw']
    
    ButtonUp        = 104
    ButtonDown      = 105
    ButtonLeft      = 106
    ButtonRight     = 107
    ButtonSession   = 108
    ButtonUser1     = 109
    ButtonUser2     = 110
    ButtonMixer     = 111
    
    PadBase         = 11 # pad  11 - 88 change to 0 - 63
    CBase           = [0, 12, 24, 36, 48, 60, 72, 84, 96, 108] # C-2, C-1...
    
    StringWaitMs    = 0
    StringDirection = -1
    
    Black           = [0, 0, 0]
    Red             = [63, 0, 0]
    Green           = [0, 63, 0]
    Blue            = [0, 0, 63]
    Yellow          = [63, 63, 0]
    Purple          = [63, 0, 63]
    Cyan            = [0, 63, 63]
    White           = [63, 63, 63]
    Grey            = [32, 32, 32]
    DarkGrey        = [16, 16, 16]
    DarkYellow      = [16, 16, 0]
    PadDefaultColor = [
        DarkYellow, Black, DarkGrey, Black, DarkGrey, DarkGrey, Black, DarkGrey,
        Black, DarkGrey, Black, DarkGrey, DarkYellow, Black, DarkGrey, Black,
        DarkGrey, DarkGrey, Black, DarkGrey, Black, DarkGrey, Black, DarkGrey,
        DarkYellow, Black, DarkGrey, Black, DarkGrey, DarkGrey, Black, DarkGrey,
        Black, DarkGrey, Black, DarkGrey, DarkYellow, Black, DarkGrey, Black,
        DarkGrey, DarkGrey, Black, DarkGrey, Black, DarkGrey, Black, DarkGrey,
        DarkYellow, Black, DarkGrey, Black, DarkGrey, DarkGrey, Black, DarkGrey,
        Black, DarkGrey, Black, DarkGrey, DarkYellow, Black, DarkGrey, Black
    ]
    
    currentMode     = ModePad
    noteBaseIndex   = 3
    noteBase        = CBase[noteBaseIndex]
    padColor        = None
    sessionPressed  = False
    
    def __init__(self, launchpad):
        self.launchpad = launchpad
        self.midiCharacteristic = None
    
    def reset(self):
        # reset launchpad button & led state
        self.launchpad.ButtonFlush()
        self.launchpad.LedAllOn(0)
    
    def setMidiCharacteristic(self, midiCharacteristic):
        self.midiCharacteristic = midiCharacteristic

    def padToNote(self, pad):
        pad -= self.PadBase
        m = math.floor(pad / 10) * 2
        return int(pad - m + self.noteBase)
    
    def NoteToPad(self, note):
        note -= self.noteBase
        m = (math.floor(note / 8) * 2)
        return int(note + m) + self.PadBase

    def press(self, state):
        if self.currentMode == self.ModePad:
            if state[0] <= 8:
                self.pressStudioPad(state)
            elif state[0] <= 80 and state[0] % 10 == 0:
                self.pressControlPad(state)
            elif state[0] <= 89 and state[0] % 10 == 9:
                self.pressAudioPad(state)
            elif state[0] >= 104:
                self.pressSessionPad(state)
            else:
                self.pressNotePad(state)
        else:
            self.padRaw(state)

    def bleWrite(self, note, velocity, color = Blue):
        note = self.NoteToPad(note)
        velocity = int(math.floor(velocity / 2)) # 0-127 to 0-63
        self.launchpad.LedCtrlRaw(*util.extend(note, color if velocity > 0 else self.Black))

    # Private
    
    def pressStudioPad(self, state):
        self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Blue))
        
    def pressControlPad(self, state):
        if state[1] == 0:
            self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Black))
        else:
            self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Green))
        
    def pressAudioPad(self, state):
        if state[1] == 0:
            self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Black))
        else:
            self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Yellow))
        
    def buttonColor(self, button, color):
        tmp = [button]
        tmp.extend(color)
        return tmp
        
    def pressSessionPad(self, state):
        if state[1] == 0:
            self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Black))
            if state[0] == self.ButtonSession:
                self.sessionPressed = False
        else:
            self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Cyan))
            
            if state[0] == self.ButtonUp or state[0] == self.ButtonDown:
                limit = False
                if state[0] == self.ButtonUp:
                    if self.noteBaseIndex < len(self.CBase) - 1:
                        self.noteBaseIndex += 1
                    else:
                        limit = True
                elif state[0] == self.ButtonDown:
                    if self.noteBaseIndex > 0:
                        self.noteBaseIndex -= 1
                    else:
                        limit = True

                self.noteBase = self.CBase[self.noteBaseIndex]
                if limit:
                    self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Red))
                    self.showMessage('C' + str(self.noteBaseIndex - 2), self.Red)
                    self.updatePadColor()
                else:
                    self.showMessage('C' + str(self.noteBaseIndex - 2), self.Yellow)
                    self.updatePadColor()
            elif state[0] == self.ButtonLeft:
                self.showMessage('C' + str(self.noteBaseIndex - 2), self.Green)
                self.updatePadColor()
            elif state[0] == self.ButtonSession:
                self.sessionPressed = True
            elif state[0] == self.ButtonUser1 and self.sessionPressed:
                self.currentMode = self.ModeRaw
                self.padColor = None
                self.updatePadColor()
                self.showMessage(self.ModeString[self.currentMode], self.Green)
            elif state[0] == self.ButtonUser2 and self.sessionPressed:
                self.currentMode = self.ModePad
                self.showMessage(self.ModeString[self.currentMode], self.Green)
            elif state[0] == self.ButtonMixer and self.sessionPressed:
                if self.currentMode == self.ModeRaw:
                    return
                
                if self.padColor is None:
                    self.padColor = self.PadDefaultColor
                else:
                    self.padColor = None
                self.updatePadColor()
        
        
    def pressNotePad(self, state):
        note = self.padToNote(state[0])
        self.midiCharacteristic.onLaunchpadUnPress(note, state[1])
        
        if state[1] == 0:
            if self.padColor is None:
                self.launchpad.LedCtrlRaw(*util.extend(state[0], self.Black))
            else:
                pad = self.padToNote(state[0]) - self.noteBase
                color = self.padColor[pad]
                self.launchpad.LedCtrlRaw(*util.extend(state[0], color))
        else:
            self.launchpad.LedCtrlRaw(state[0], 63, 63, 63)
            # Sample
            # self.launchpad.LedCtrlBpm(200)
            # self.launchpad.LedCtrlFlashByCode(state[0], 3)
            # self.launchpad.LedCtrlPulseByCode(state[0], 2)
            # self.launchpad.LedCtrlString("Mookie", 63, 0, 0, -1, 10)
    
    def padRaw(self, state):
        # send everything through BLE
        self.midiCharacteristic.onLaunchpadUnPress(state[0], state[1])
        
        # check if mode change
        if state[0] == self.ButtonSession:
            self.sessionPressed = True if state[1] > 0 else False
        elif self.sessionPressed and state[1] > 0:
            if state[0] == self.ButtonUser1:
                self.currentMode = self.ModeRaw
                self.showMessage(self.ModeString[self.currentMode], self.Green)
            elif state[0] == self.ButtonUser2:
                self.currentMode = self.ModePad
                self.showMessage(self.ModeString[self.currentMode], self.Green)
            
    # helpers
    
    def updatePadColor(self):
        if self.padColor is None:
            self.launchpad.LedAllOn(0)
        else:
            for i in range(len(self.padColor)):
                pad = self.NoteToPad(i + self.noteBase)
                self.launchpad.LedCtrlRaw(*util.extend(pad, self.padColor[i]))
                
    def showMessage(self, message, color):
        self.launchpad.LedCtrlString(message, color[0], color[1], color[2], self.StringDirection, self.StringWaitMs)
        self.updatePadColor()
    
    # Status from BLE
    
    def launchpadConnected(self):
        self.launchpad.LedCtrlRaw(*util.extend(self.ButtonSession, self.Yellow))
        time.wait(200)
    
    def bleAdvertised(self):
        self.launchpad.LedCtrlRaw(*util.extend(self.ButtonSession, self.Green))
        time.wait(200)
        self.launchpad.LedCtrlRaw(*util.extend(self.ButtonSession, self.Black))

    def bleError(self):
        self.launchpad.LedCtrlRaw(*util.extend(self.ButtonSession, self.Red))
        time.wait(200)
        
    def bleSubscribe(self):
        self.launchpad.LedCtrlRaw(*util.extend(self.ButtonUser1, self.Green))
        time.wait(150)
        self.launchpad.LedCtrlRaw(*util.extend(self.ButtonUser1, self.Black))
        
    def bleUnsubscribe(self):
        self.launchpad.LedCtrlRaw(*util.extend(self.ButtonUser1, self.Yellow))
        time.wait(150)
        self.launchpad.LedCtrlRaw(*util.extend(self.ButtonUser1, self.Black))
        
    def notePadCheckUp(self):
        notes = range(36, 100)
        for i in notes:
            self.bleWrite(i, 127, self.White)
            time.wait(10)
            self.bleWrite(i, 0)
