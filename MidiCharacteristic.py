from pybleno import *
import array
import sys
import subprocess
import re
import time
from MidiDescriptor import *

class MidiCharacteristic(Characteristic):
    STATUS = 0x90
    CHANNELMASK = 0x0F

    def __init__(self):
        self.launchpadState = None
        self.channel = 0
        
        Characteristic.__init__(self, {
            'uuid': '7772E5DB-3868-4112-A1A9-F2669D106BF3',
            'properties': ['read', 'writeWithoutResponse', 'notify'],
            'value': None,
            'descriptors': [
                  MidiDescriptor({
                    'uuid': '2901',
                    'value': 'BLE Midi by Tom'
                  })
                ]
          })
        
        self._value = [0] * 0
        self._updateValueCallback = None
        
    def setLaunchpadState(self, launchpadState):
        self.launchpadState = launchpadState
        self.launchpadState.setMidiCharacteristic(self)
    
    def onLaunchpadPress(self, note, velocity = 127):        
        if self.updateValueCallback is None:
            return
                
        timestamp_ms = None
        if sys.version_info[0] < 3:
            timestamp = "%.9f" % time.time()
            timestamp = timestamp.replace('.', '', 1)
            timestamp_ns = int(timestamp)
            timestamp_ms = timestamp_ns // 1000000
        else:
            timestamp_ms = time.monotonic_ns() // 1000000
        
        data = array.array('B', [0] * 5)
        writeUInt8(data, (timestamp_ms >> 7 & 0x3F) | 0x80, 0)
        writeUInt8(data, 0x80 | (timestamp_ms & 0x7F), 1)
        writeUInt8(data, self.STATUS | (self.channel & self.CHANNELMASK), 2)
        writeUInt8(data, note, 3)
        writeUInt8(data, velocity, 4)
        
        self._value = data
        
        try:
            self.updateValueCallback(data)    
        except:
            print('except')
    
    def onLaunchpadUnPress(self, note, velocity = 0):
        self.onLaunchpadPress(note, velocity)
          
    def onReadRequest(self, offset, callback):
        print('onReadRequest')
        callback(Characteristic.RESULT_SUCCESS, [98])
            
    def onWriteRequest(self, data, offset, withoutResponse, callback):
        # print 'onWriteRequest', offset, [hex(i) for i in data[0:2]], [i for i in data[2:]]
        if len(data) < 4:
            print 'invalid BLE package?', [hex(i) for i in data]
            callback(Characteristic.RESULT_SUCCESS)
        elif len(data) < 5:
            print 'invalid BLE package?', [hex(i) for i in data[0:2]], data[2] & self.CHANNELMASK, [i for i in data[3:]]
            callback(Characteristic.RESULT_SUCCESS)

        timestampHigh = data.pop(0)
        timestampLow = data.pop(0)
        status = data.pop(0)
        channel = status & self.CHANNELMASK
                
        if channel != self.channel:
            print 'incorrect channel', channel, [i for i in data]
            callback(Characteristic.RESULT_SUCCESS)
            return
            
        l = len(data)
        if l == 0 or l % 2 != 0:
            print 'invalid BLE data?', channel, [i for i in data]
            callback(Characteristic.RESULT_SUCCESS)
        
        print 'recv', channel, [i for i in data]
        
        for i in range(l / 2):
            note = data[i * 2]
            velocity = data[i * 2 - 1]
            self.launchpadState.bleWrite(note, velocity)

        callback(Characteristic.RESULT_SUCCESS)

    def onSubscribe(self, maxValueSize, updateValueCallback):
        # print('onSubscribe')
        self.launchpadState.bleSubscribe()
        self.maxValueSize = maxValueSize
        self.updateValueCallback = updateValueCallback

    def onUnsubscribe(self):
        print('onUnsubscribe')
        self.launchpadState.bleUnsubscribe()
        self.maxValueSize = None
        self.updateValueCallback = None

    def onNotify(self):
        pass
        # print('onNotify')

    def onIndicate(self):
        print('onIndicate')
